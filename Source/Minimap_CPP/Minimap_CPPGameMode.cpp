// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Minimap_CPP.h"
#include "Minimap_CPPGameMode.h"
#include "Minimap_CPPHUD.h"
#include "Minimap_CPPCharacter.h"

AMinimap_CPPGameMode::AMinimap_CPPGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMinimap_CPPHUD::StaticClass();
}
